#include <stdio.h>
#include "LPC17xx.h"

//P3.25
//P3.26

#define LED0_BIT_POS 25
#define LED1_BIT_POS 26

int main(void)
{
	int i;
	LPC_GPIO3->FIODIR |= (1 << LED0_BIT_POS);
	LPC_GPIO3->FIOCLR |= (1 << LED0_BIT_POS);
	while(1)
	{
		LPC_GPIO3->FIOCLR |= (1 << LED0_BIT_POS);
		for (i = 0; i < 0xFFFF ; i++);
		LPC_GPIO3->FIOSET |= (1 << LED0_BIT_POS);
		for (i = 0; i < 0xFFFF ; i++);
	}
	return 0;
}
