//
// Basic LED-blink for the MBED.
//
#include "LPC17xx.h"
#include <stdint.h>

#define LED0_PIN (25)

int main() {

  SystemInit();

    // Setup P1.23 as output
  LPC_GPIO3->FIODIR |= (1 << LED0_PIN);

  for(;;) {
    for(uint32_t delay = 0; delay < 10000000; delay++) {
       __asm("NOP");
    }

    // Turn LED ON
    LPC_GPIO3->FIOSET = (1 << LED0_PIN);

    for(uint32_t delay = 0; delay < 10000000; delay++) {
       __asm("NOP");
    }

    // Turn LED OFF
    LPC_GPIO3->FIOCLR = (1 << LED0_PIN);
  }
}
