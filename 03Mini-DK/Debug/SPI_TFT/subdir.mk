################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../SPI_TFT/GraphicsDisplay.cpp \
../SPI_TFT/SPI_TFT.cpp \
../SPI_TFT/TextDisplay.cpp 

OBJS += \
./SPI_TFT/GraphicsDisplay.o \
./SPI_TFT/SPI_TFT.o \
./SPI_TFT/TextDisplay.o 

CPP_DEPS += \
./SPI_TFT/GraphicsDisplay.d \
./SPI_TFT/SPI_TFT.d \
./SPI_TFT/TextDisplay.d 


# Each subdirectory must supply rules for building sources it contributes
SPI_TFT/%.o: ../SPI_TFT/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DTOOLCHAIN_GCC_CR -DTARGET_LPC1768 -D__CODE_RED -DCPP_USE_HEAP -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TouchADS7843" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TFT_fonts" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\SPI_TFT" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768\GCC_CR" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


