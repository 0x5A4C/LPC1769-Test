################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../main.cpp 

C_SRCS += \
../graphics.c 

OBJS += \
./graphics.o \
./main.o 

C_DEPS += \
./graphics.d 

CPP_DEPS += \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DTOOLCHAIN_GCC_CR -DTARGET_LPC1768 -D__CODE_RED -DCPP_USE_HEAP -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TouchADS7843" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TFT_fonts" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\SPI_TFT" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768\GCC_CR" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DTOOLCHAIN_GCC_CR -DTARGET_LPC1768 -D__CODE_RED -DCPP_USE_HEAP -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TouchADS7843" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\TFT_fonts" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\SPI_TFT" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768" -I"C:\Users\CAD\Documents\LPCXpresso_4.3.0_1023\workspace\Mini-DK\mbed\LPC1768\GCC_CR" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


