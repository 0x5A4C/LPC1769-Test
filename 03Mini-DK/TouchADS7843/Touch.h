
#ifndef MBED_Touch_H
#define MBED_Touch_H

#include "SPI_TFT.h"
#include "mbed.h"

	typedef struct
    {
       int x;
       int y;
    } Coordinate;

    typedef struct
    {
    int         An,
                Bn,
                Cn,
                Dn,
                En,
                Fn,
                Divider ;
    } Matrix;

class TouchScreenADS7843 : public  SPI_TFT
{
public:
//    extern Coordinate ScreenSample[3];
//    extern Coordinate DisplaySample[3];
//    extern Matrix matrix ;
//    extern Coordinate  display ;
//    Coordinate ScreenSample[3];
//    Coordinate DisplaySample[3];
//    Matrix matrix ;
//    Coordinate  display ;

    TouchScreenADS7843(PinName tp_mosi,PinName tp_miso,PinName tp_sclk,PinName tp_cs,PinName tp_irq,PinName mosi, PinName miso, PinName sclk, PinName cs, PinName reset,const char* name ="TFT");

    void TP_Init(void);
    void TP_GetAdXY(int *x,int *y);
    void TP_DrawPoint(unsigned int Xpos,unsigned int Ypos,unsigned int color);
//    Coordinate Read_Ads7846(void);
    unsigned char Read_Ads7846(Coordinate * screenPtr);
    void TouchPanel_Calibrate(Matrix * matrixPtr);
    unsigned char getDisplayPoint(Coordinate * displayPtr,Coordinate * screenPtr,Matrix * matrixPtr );

    SPI        _tp_spi;
    DigitalOut _tp_cs;
    DigitalIn  _tp_irq;

protected:

#define SPI_RD_DELAY 1
#define	CHX 	        0xd0	// 12 bit mode
#define	CHY 	        0x90

    int Read_XY(unsigned char XY);
    void DrawCross(unsigned int Xpos,unsigned int Ypos);
    unsigned char setCalibrationMatrix( Coordinate * displayPtr,Coordinate * screenPtr,Matrix * matrixPtr);

};
#endif
