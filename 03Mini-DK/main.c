#include "stdio.h"
#include "mbed.h"
#include "SPI_TFT.h"
#include "string"
#include "Arial12x12.h"
#include "Arial24x23.h"
#include "Arial28x28.h"
#include "font_big.h"
#include "Touch.h"

extern unsigned char p1[];  // the mbed logo

// ADS7843 -> mosi, miso, sclk, cs, irq  -  TFT -> mosi, miso, sclk, cs, reset
TouchScreenADS7843 TFT(p5 ,p6 ,p7 ,p8 ,P2_13 ,p11, p12, p13, p14, p15,"TFT");

Matrix      matrix;
Coordinate  display;
Coordinate  screen;

int main()
{
	unsigned char Ads7846_status;
	unsigned short LCD_id;
	TFT.claim(stdout);        // send stdout to the TFT display
    TFT.TP_Init();

    TFT.background(Black);    // set background to black
    TFT.foreground(White);    // set chars to white


    // LCD demo
    // first show the 4 directions
    TFT.cls();
    TFT.set_font((unsigned char*) Arial12x12);
    TFT.set_orientation(0);
    TFT.locate(0,0);
    printf("  Hello Mbed 0");
    TFT.set_orientation(1);
    TFT.locate(0,0);
    printf("  Hello Mbed 1");
    TFT.set_orientation(2);
    TFT.locate(0,0);
    printf("  Hello Mbed 2");
    TFT.set_orientation(3);
    TFT.locate(0,0);
    printf("  Hello Mbed 3");
    TFT.set_orientation(1);
    TFT.set_font((unsigned char*) Arial24x23);
    TFT.locate(50,100);
    TFT.printf("TFT orientation");

    wait(2);

    // draw some graphics
    TFT.cls();
    TFT.set_orientation(1);
    TFT.set_font((unsigned char*) Arial24x23);
    TFT.locate(120,115);
    TFT.printf("Graphic");
    TFT.line(0,0,100,200,Green);
    TFT.rect(100,50,150,100,Red);
    TFT.fillrect(180,25,220,70,Blue);
    TFT.circle(80,150,33,White);
    TFT.fillcircle(80,50,33,White);

    wait(2);

    // bigger text
    TFT.foreground(White);
    TFT.background(Blue);
    TFT.cls();
    TFT.set_font((unsigned char*) Arial24x23);
    TFT.locate(0,0);
    TFT.printf("Different Fonts :");

    TFT.set_font((unsigned char*) Neu42x35);
    TFT.locate(0,50);
    TFT.printf("Hello");
    TFT.set_font((unsigned char*) Arial24x23);
    TFT.locate(50,100);
    TFT.printf("Hello");
    TFT.set_font((unsigned char*) Arial12x12);
    TFT.locate(55,150);
    TFT.printf("Hello");

    TFT.set_orientation(2);
    TFT.set_font((unsigned char*) Arial24x23);
    TFT.locate(10,10);
    TFT.printf("Hi mbed");
    wait(2);

    // mbed logo
    TFT.set_orientation(1);
    TFT.background(Black);
    TFT.cls();
    TFT.Bitmap(90,90,172,55,p1);

    // Read LCD ID
    TFT.set_orientation(0);
    LCD_id = TFT.Read_ID();
    TFT.locate(10,10);
    TFT.printf("LCD: ILI%04X", LCD_id);
    wait(2);



    // Touchpanel demo
    TFT.TouchPanel_Calibrate(&matrix);
    TFT.set_font((unsigned char*) Arial12x12);
    TFT.set_orientation(0);
    TFT.cls();
    TFT.locate(0,0);
    TFT.printf(" X:");
    TFT.locate(70,0);
    TFT.printf(" Y:");

    while (1)
    {
    	if (!TFT._tp_irq)
    	{
    		Ads7846_status = TFT.Read_Ads7846(&screen);
    		if (Ads7846_status)
    		{
    	    	TFT.getDisplayPoint(&display, &screen, &matrix ) ;
    	    	TFT.TP_DrawPoint(display.x,display.y, Blue);
//    	    	TFT.rect(display.x,display.y,display.x+1,display.y+1,Red);
    	    	TFT.locate(25,0);
    	    	printf("%03d",display.x);
    	    	TFT.locate(95,0);
    	    	printf("%03d",display.y);
    	    	// Touchscreen area is larger than LCD area.
    	    	// We use the bottom area outside the LCD area to clear the screen (y value > 320).
    	    	if (display.y > 320)
    	    	{
    	    	    TFT.cls();
    	    	    TFT.locate(0,0);
    	    	    TFT.printf(" X:");
    	    	    TFT.locate(70,0);
    	    	    TFT.printf(" Y:");

    	    	}
    		}
    	}
    }
}
