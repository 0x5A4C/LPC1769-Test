import os 
from fabricate import *

setup(dirs=['.', '../lib'])

#COMPILER = '/home/ziemek/Tools/gcc-arm-none-eabi-6_2-2016q4/bin/arm-none-eabi-gcc'
#OBJCOPY = '/home/ziemek/Tools/gcc-arm-none-eabi-6_2-2016q4/bin/arm-none-eabi-objcopy'

COMPILER = '/home/ziemek/Tools/gcc-arm-none-eabi-6-2017-q2-update/bin/arm-none-eabi-gcc'
OBJCOPY = '/home/ziemek/Tools/gcc-arm-none-eabi-6-2017-q2-update/bin/arm-none-eabi-objcopy'

TARGET = 'program'
SOURCES = ['startup', 'main']
CFLAGS = '-mthumb -mcpu=cortex-m3 -O1 -std=c99 -g'.split()
LFLAGS = '-nostartfiles -mthumb -mcpu=cortex-m3 -O1 -std=c99 -g -T lpc1769.ld'.split()
OBFLAGS = '-O binary'.split()

def build():
    """
    Build project.
    """
    version()
    compile()
    link()
    objcpy()

def flash():
    """
    Flash target
    """
    shell("openocd -f interface/jlink.cfg -f target/lpc17xx.cfg -f flashTarget.cfg".split())

def version():
    """
    Generate version define in version.h file.
    """
    revision = shell('git', 'rev-list --count HEAD'.split()).strip()
    print >>file('version.h', 'w'), '#define REVISION "%s"' % revision

def oname(build_dir, filename):
    """
    Privet function used form combining file name and basedir.
    """
    return os.path.join(build_dir, os.path.basename(filename))

def compile(build_dir='build', flags=None):
    """
    compile
    """
    for source in SOURCES:
        run(COMPILER, '-c', source+'.c', '-o', oname(build_dir, source+'.o'), CFLAGS, flags)

def link(build_dir='build', flags=None):
    """
    Link.
    """
    objects = [oname(build_dir, s+'.o') for s in SOURCES]
    run(COMPILER, objects, '-o', oname(build_dir, TARGET+'.axf'), LFLAGS, flags)

def objcpy(build_dir='build', flags=None):
    """
    Generate bin.
    """
    run(OBJCOPY, OBFLAGS, oname(build_dir, TARGET+'.axf'), oname(build_dir, TARGET+'.bin'), flags)

def check():
    """
    Return list of changed files.
    """
    return int(outofdate(build))

def clean():
    """
    Remove...
    """
    autoclean()

def rebuild():
    """
    Clean and build.
    """
    clean()
    build()

main()
